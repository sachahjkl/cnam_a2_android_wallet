package it.hjkl.wallet.util

import kotlin.math.round

object Utilities {

    fun roundToPrecision(value: Double, decimals: Int = 1): Double {
        val d = if (decimals < 0) 0 else decimals
        var multiplier = 1.0
        repeat(d) { multiplier *= 10 }
        return round(value * multiplier) / multiplier
    }
}