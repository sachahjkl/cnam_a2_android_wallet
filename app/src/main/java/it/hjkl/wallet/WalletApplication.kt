package it.hjkl.wallet

import android.app.Application
import it.hjkl.wallet.viewmodel.MainRepository
import it.hjkl.wallet.api.RetrofitInstance
import it.hjkl.wallet.db.WalletDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class WalletApplication : Application() {

    // No need to cancel this scope as it'll be torn down with the process

    private val applicationScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    private val database by lazy { WalletDatabase.invoke(this, applicationScope) }
    val repository by lazy { MainRepository(RetrofitInstance.api, database.addressDao()) }

}