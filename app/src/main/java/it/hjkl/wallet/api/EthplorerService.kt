package it.hjkl.wallet.api

import it.hjkl.wallet.model.AddressInfo
import it.hjkl.wallet.model.Operations
import it.hjkl.wallet.model.TokenInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EthplorerService {

    /**
     * Fonction permettant de requêter les infos sur un token
     * @param address l'addresse du token pour lequel on veut récupérer les infos
     * @param apiKey la clé d'API (supposement secrète)
     * @return une objet Response encapsulant l'état du succès de la requête ainsi que le
     * résultat si la requête a abouti
     */
    @GET("getTokenInfo/{address}")
    suspend fun getTokenInfo(
        @Path("address", encoded = false)
        address: String,
        @Query("apiKey")
        apiKey: String = Constants.API_KEY
    ): Response<TokenInfo>

    /**
     * Fonction permettant de requêter les infos sur une adresse (portefeuille)
     * @param address l'addresse pour laquelle on veut récupérer les infos
     * @param apiKey la clé d'API (supposement secrète)
     * @return une objet Response encapsulant l'état du succès de la requête ainsi que le
     * résultat si la requête a abouti
     */
    @GET("getAddressInfo/{address}")
    suspend fun getAddressInfo(
        @Path("address", encoded = false)
        address: String,
        @Query("apiKey")
        apiKey: String = Constants.API_KEY
    ): Response<AddressInfo>

    /**
     * Fonction permettant de requêter les opérations efféctuées par une adresse pour un certain token
     * @param address l'addresse pour laquelle on veut les opérations
     * @param apiKey la clé d'API (supposement secrète)
     * @param token l'adresse du token pour lequel on veut les opérations
     * @return une objet Response encapsulant l'état du succès de la requête ainsi que le
     * résultat si la requête a abouti
     */
    @GET("getAddressHistory/{address}")
    suspend fun getAddressHistory(
        @Path("address", encoded = false)
        address: String,
        @Query("apiKey")
        apiKey: String = Constants.API_KEY,
        @Query("token")
        token: String
    ): Response<Operations>

}