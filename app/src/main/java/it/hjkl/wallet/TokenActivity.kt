package it.hjkl.wallet

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import it.hjkl.wallet.adapter.LinksListAdapter
import it.hjkl.wallet.databinding.ActivityTokenBinding
import it.hjkl.wallet.model.Token
import it.hjkl.wallet.util.Utilities.roundToPrecision

class TokenActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityTokenBinding
    private val binding get() = _binding

    private lateinit var token: Token
    private lateinit var adapter: LinksListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityTokenBinding.inflate(layoutInflater)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = LinksListAdapter()
        binding.links.adapter = adapter
        adapter.setOnClickListener { viewUri(it) }

        token = intent.extras?.getSerializable("token") as Token

        setContentView(binding.root)


        setUI(token)

    }

    private fun setUI(token: Token) {
        with(binding) {
            val roundedBalanceUSD = roundToPrecision(token.value, 2)
            val imageUrl = getString(R.string.base_url_ethplorer) + token.tokenInfo.image
            val links = getLinks(token)
            val fullName = token.tokenInfo.fullName
            label.text =
                if (fullName.isBlank()) getString(R.string.no_name_token) else fullName
            address.text = token.tokenInfo.address

            if (!token.tokenInfo.website.isNullOrBlank()) {
                website.setOnClickListener {
                    viewUri(token.tokenInfo.website)
                }
            } else {
                website.isVisible = false
            }
            token.tokenInfo.image?.let {
                Glide.with(root)
                    .load(imageUrl)
                    .into(logo)
            }

            valeur.text =
                getString(R.string.balance, roundedBalanceUSD.toString())
            solde.text = roundToPrecision(token.balanceAdjusted, 2).toString()


            adapter.differ.submitList(links)

        }

    }

    private fun getLinks(token: Token): MutableList<LinksListAdapter.Link> {
        val links = mutableListOf<LinksListAdapter.Link>()

        val ethplorerUrl = getString(R.string.base_url_ethplorer_token, token.tokenInfo.address)

        links.add(
            LinksListAdapter.Link(
                ethplorerUrl,
                AppCompatResources.getDrawable(applicationContext, R.drawable.ethplorer),
                getString(R.string.ethplorer_title)
            )
        )

        if (!token.tokenInfo.telegram.isNullOrBlank()) {
            links.add(
                LinksListAdapter.Link(
                    token.tokenInfo.telegram,
                    AppCompatResources.getDrawable(applicationContext, R.drawable.ic_telegram),
                    "${getString(R.string.telegram)} - ${token.tokenInfo.telegram}"
                )
            )

        }
        if (!token.tokenInfo.twitter.isNullOrBlank()) {
            val twitterUrl = getString(R.string.base_url_twitter, token.tokenInfo.twitter)
            links.add(
                LinksListAdapter.Link(
                    twitterUrl,
                    AppCompatResources.getDrawable(applicationContext, R.drawable.ic_twitter),
                    "${getString(R.string.twitter)} - ${token.tokenInfo.twitter}"
                )
            )

        }
        if (!token.tokenInfo.reddit.isNullOrBlank()) {
            val redditUrl = getString(R.string.base_url_reddit, token.tokenInfo.reddit)
            links.add(
                LinksListAdapter.Link(
                    redditUrl,
                    AppCompatResources.getDrawable(applicationContext, R.drawable.ic_reddit),
                    "${getString(R.string.reddit)} - ${token.tokenInfo.reddit}"
                )
            )
        }

        return links
    }

    private fun viewUri(uri: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        startActivity(intent)
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}