package it.hjkl.wallet.db

import androidx.room.*
import it.hjkl.wallet.db.entitity.Address
import kotlinx.coroutines.flow.Flow

@Dao
interface AddressDao {

    @Query("SELECT * FROM Address")
    fun getAddresses(): Flow<List<Address>>


    // Insert and Update en un seul
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(address: Address): Long


    @Query("DELETE FROM Address WHERE address = :address")
    suspend fun deleteByAddress(address: String)

    @Query("DELETE FROM Address")
    suspend fun deleteAll()


}