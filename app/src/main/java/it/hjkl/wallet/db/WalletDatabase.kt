package it.hjkl.wallet.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import it.hjkl.wallet.db.entitity.Address
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Database(
    entities = [Address::class],
    version = 1,
    exportSchema = false
)
abstract class WalletDatabase : RoomDatabase() {

    abstract fun addressDao(): AddressDao

    companion object {
        @Volatile
        private var INSTANCE: WalletDatabase? = null
        private val LOCK = Any()


        operator fun invoke(context: Context, scope: CoroutineScope): WalletDatabase =
            INSTANCE ?: synchronized(LOCK) {
                INSTANCE ?: buildDatabase(context, scope).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context, scope: CoroutineScope) =
            Room.databaseBuilder(
                context,
                WalletDatabase::class.java, "wallet.db"
            )
                .fallbackToDestructiveMigration()
                .addCallback(WalletDatabaseCallback(scope))
                .build()

    }


    // Pour peupler la base de données avec des données de test
    private class WalletDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)

            INSTANCE?.let { _ ->
                scope.launch {
//                    val dao = database.addressDao()
//                    dao.deleteAll()

//                    dao.insert(
//                        Address(
//                            "0xDfB091f812ea27Ca58e8f556B252f245660cba87",
//                            "Mon Wallet"
//                        )
//                    )
                }
            }
        }
    }


}