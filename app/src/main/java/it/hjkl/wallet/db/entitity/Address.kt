package it.hjkl.wallet.db.entitity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Address(
    @PrimaryKey val address: String,
    val label: String
)  : Serializable