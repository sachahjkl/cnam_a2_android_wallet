package it.hjkl.wallet

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import it.hjkl.wallet.adapter.OperationsListAdapter
import it.hjkl.wallet.databinding.ActivityOperationsBinding
import it.hjkl.wallet.db.entitity.Address
import it.hjkl.wallet.model.Operation
import it.hjkl.wallet.model.Token
import it.hjkl.wallet.util.Utilities.roundToPrecision
import it.hjkl.wallet.viewmodel.OperationsViewModel
import kotlinx.coroutines.flow.collect

class OperationsActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityOperationsBinding
    private val binding get() = _binding
    private val viewModel: OperationsViewModel by viewModels {
        OperationsViewModel.OperationsViewModelFactory((application as WalletApplication).repository)
    }

    private lateinit var token: Token
    private lateinit var address: Address
    private lateinit var adapter: OperationsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityOperationsBinding.inflate(layoutInflater)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        token = intent.extras?.getSerializable("token") as Token
        address = intent.extras?.getSerializable("address") as Address
        adapter = OperationsListAdapter(address)

        binding.rvOperations.adapter = adapter
        adapter.setOnClickListener { viewUri(it) }


        setContentView(binding.root)

        lifecycleScope.launchWhenStarted {
            viewModel.fetchOperations(address.address, token.tokenInfo.address.toString()).also {
                viewModel.operationListState.collect { state ->
                    updateUI(state)
                }
            }
        }
    }

    private fun viewUri(uri: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        startActivity(intent)
    }

    private fun updateUI(state: OperationsViewModel.OperationListState) {
        when (state) {
            is OperationsViewModel.OperationListState.Loading -> {
                setNoOperations(false)
                setLoading(true)
            }
            is OperationsViewModel.OperationListState.Success -> {
                setNoOperations(false)
                setLoading(false)
                setUI(state.operations)
            }
            is OperationsViewModel.OperationListState.Failure -> {
                setNoOperations(false)
                setLoading(false)
                AlertDialog.Builder(this)
                    .setMessage(state.errorText)
                    .setNegativeButton("Retour", null)
                    .setOnDismissListener { finish() }
                    .create().show()
            }
            is OperationsViewModel.OperationListState.Empty -> {
                setNoOperations(true)
                setLoading(false)
            }
        }
    }

    private fun setUI(operations: List<Operation>) {
        with(binding) {
            val ethplorerUrl =
                getString(
                    R.string.base_url_ethplorer_transactions,
                    this@OperationsActivity.token.tokenInfo.address,
                    this@OperationsActivity.address.address
                )
            val fullName = token.tokenInfo.fullName
            val imageUrl = getString(R.string.base_url_ethplorer) + token.tokenInfo.image


            solde.text = roundToPrecision(token.balanceAdjusted, 2).toString()
            nbOperations.text = operations.size.toString()
            label.text =
                if (fullName.isBlank()) getString(R.string.no_name_token) else fullName
            address.text = token.tokenInfo.address

            token.tokenInfo.image?.let {
                Glide.with(root)
                    .load(imageUrl)
                    .into(logo)
            }

            ethplorer.setOnClickListener {
                viewUri(ethplorerUrl)
            }

            adapter.differ.submitList(operations)
        }

    }


    private fun setNoOperations(noTokens: Boolean = true) {
        binding.apply {
            emptyText.isVisible = noTokens
        }
    }

    private fun setLoading(loading: Boolean = true) {
        binding.apply {
            summary.isVisible = !loading
            rvOperations.isVisible = !loading
            progressBar.isVisible = loading
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}