package it.hjkl.wallet.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import it.hjkl.wallet.model.Operation
import it.hjkl.wallet.model.Operations
import it.hjkl.wallet.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield

class OperationsViewModel(private val repository: MainRepository) : ViewModel() {

    private val _operationListState = MutableStateFlow<OperationListState>(OperationListState.Empty)
    val operationListState: StateFlow<OperationListState> = _operationListState

    sealed class OperationListState {
        class Success(val operations: List<Operation>) : OperationListState()
        class Failure(val errorText: String) : OperationListState()
        object Loading : OperationListState()
        object Empty : OperationListState()
    }

    fun fetchOperations(address: String, token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _operationListState.value = OperationListState.Loading
            when (val resource = repository.getTransactions(address, token)) {
                is Resource.Error -> {
                    _operationListState.value =
                        OperationListState.Failure(resource.message ?: "Erreur inconnue")
                }
                is Resource.Success -> {
                    val data = resource.data
                    if (data == null) {
                        _operationListState.value =
                            OperationListState.Failure(resource.message ?: "Erreur inconnue")
                    } else {
                        _operationListState.value =
                            if (data.operations.isNullOrEmpty())
                                OperationListState.Empty
                            else
                                OperationListState.Success(resource.data.operations)
                    }
                }
            }
        }
    }


    class OperationsViewModelFactory(private val repository: MainRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(OperationsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return OperationsViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }


}