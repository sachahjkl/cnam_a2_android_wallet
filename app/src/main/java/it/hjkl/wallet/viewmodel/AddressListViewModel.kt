package it.hjkl.wallet.viewmodel

import androidx.lifecycle.*
import it.hjkl.wallet.db.entitity.Address
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AddressListViewModel(private val repository: MainRepository) : ViewModel() {

    private val _addresses = MutableStateFlow<AddressListState>(AddressListState.Empty)
    val addressesState get() = _addresses

    sealed class AddressListState {
        class Success(val addresses: List<Address>) : AddressListState()
        class Failure(val errorText: String) : AddressListState()
        object Loading : AddressListState()
        object Empty : AddressListState()
    }

    init {
        viewModelScope.launch(Dispatchers.IO) {
            _addresses.value = AddressListState.Loading
            repository.getAllAddresses().collect { addresses ->
                _addresses.value = AddressListState.Success(addresses)
            }
        }
    }


    fun insertAddress(address: Address) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertAddress(address)
        }
    }

    fun deleteAddress(address: Address) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAddress(address.address)
        }
    }

    class AddressListViewModelFactory(private val repository: MainRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddressListViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return AddressListViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

}