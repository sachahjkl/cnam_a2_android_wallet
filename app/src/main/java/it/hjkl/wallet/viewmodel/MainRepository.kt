package it.hjkl.wallet.viewmodel

import it.hjkl.wallet.api.EthplorerService
import it.hjkl.wallet.db.AddressDao
import it.hjkl.wallet.db.entitity.Address
import it.hjkl.wallet.model.AddressInfo
import it.hjkl.wallet.model.Operations
import it.hjkl.wallet.util.Resource
import kotlinx.coroutines.flow.Flow
import java.lang.Exception


class MainRepository(private val api: EthplorerService, private val addressDao: AddressDao) {

    companion object {
        const val UNKNOWN_ERROR = "Erreur inconnue"
        const val NETWORK_FAIL = "Erreur réseau"
    }

    suspend fun getAddressInfo(address: String): Resource<AddressInfo> {
        return try {
            val response = api.getAddressInfo(address)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(NETWORK_FAIL)
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: UNKNOWN_ERROR)
        }
    }

    @Suppress("unused")
    suspend fun getOperations(address: String, token: String): Resource<Operations> {
        return try {
            val response = api.getAddressHistory(address = address, token = token)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(NETWORK_FAIL)
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: UNKNOWN_ERROR)
        }
    }

    suspend fun getTransactions(address: String, token: String) : Resource<Operations> {
        return try {
            val response = api.getAddressHistory(address = address, token = token)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                val transfers = result.operations.filter {
                    it.type == "transfer"
                }
                result.operations = transfers
                Resource.Success(result)
            } else {
                Resource.Error(NETWORK_FAIL)
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: UNKNOWN_ERROR)
        }
    }

    fun getAllAddresses(): Flow<List<Address>> = addressDao.getAddresses()

    suspend fun insertAddress(address: Address): Resource<Long> {
        return try {
            val id = addressDao.insert(address)
            if (id < 0)
                return Resource.Error("Echec à l'insertion")
            Resource.Success(id)
        } catch (e: Exception) {
            Resource.Error(e.message ?: "Echec à l'insertion")
        }

    }

    suspend fun deleteAddress(address: String): Resource<String> {
        return try {
            addressDao.deleteByAddress(address)
            Resource.Success("byebye")
        } catch (e: Exception) {
            Resource.Error(e.message ?: "Echec à l'insertion")
        }
    }

}