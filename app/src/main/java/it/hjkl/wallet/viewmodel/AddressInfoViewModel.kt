package it.hjkl.wallet.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import it.hjkl.wallet.model.AddressInfo
import it.hjkl.wallet.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class AddressInfoViewModel(private val repository: MainRepository) : ViewModel() {

    private val _addressInfoState = MutableStateFlow<AddressInfoState>(AddressInfoState.Empty)
    val addressInfoState: StateFlow<AddressInfoState> = _addressInfoState

    sealed class AddressInfoState {
        class Success(val addressInfo: AddressInfo) : AddressInfoState()
        class Failure(val errorText: String) : AddressInfoState()
        object Loading : AddressInfoState()
        object Empty : AddressInfoState()
    }

    fun fetchAddressInfo(address: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _addressInfoState.value = AddressInfoState.Loading
            when (val resource = repository.getAddressInfo(address)) {
                is Resource.Error -> {
                    _addressInfoState.value =
                        AddressInfoState.Failure(resource.message ?: "Erreur inconnue")
                }
                is Resource.Success -> _addressInfoState.value =
                    AddressInfoState.Success(resource.data!!)
            }
        }
    }


    class AddressInfoViewModelFactory(private val repository: MainRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddressInfoViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return AddressInfoViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }


}