package it.hjkl.wallet.adapter


import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.hjkl.wallet.R
import it.hjkl.wallet.databinding.ListItemOperationBinding
import it.hjkl.wallet.db.entitity.Address
import it.hjkl.wallet.model.Operation
import it.hjkl.wallet.util.Utilities.roundToPrecision
import java.time.Instant
import java.time.ZoneId

class OperationsListAdapter(private val address: Address) :
    RecyclerView.Adapter<OperationsListAdapter.TokenViewHolder>() {

    inner class TokenViewHolder(val binding: ListItemOperationBinding) :
        RecyclerView.ViewHolder(binding.root)

    private val differCallback = object : DiffUtil.ItemCallback<Operation>() {
        override fun areItemsTheSame(
            oldItem: Operation,
            newItem: Operation
        ): Boolean {
            return oldItem.transactionHash == newItem.transactionHash
        }

        override fun areContentsTheSame(
            oldItem: Operation,
            newItem: Operation
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TokenViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemOperationBinding.inflate(layoutInflater, parent, false)
        return TokenViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TokenViewHolder, position: Int) {
        val currentOperation = differ.currentList[position]

        holder.binding.apply {
            val amountAdjusted = roundToPrecision(currentOperation.valueAdjusted, 2)
            val dateFormatted =
                Instant.ofEpochSecond(currentOperation.timestamp).atZone(ZoneId.systemDefault())
                    .toLocalDateTime().toString()

            val transactionUrl = root.context.getString(
                R.string.base_url_ethplorer_transaction,
                currentOperation.transactionHash
            )

            amount.text = amountAdjusted.toString()
            from.text = currentOperation.from
            to.text = currentOperation.to
            hash.text = currentOperation.transactionHash
            symbol.text = currentOperation.tokenInfo.symbol
            with(currentOperation) {
                transacStatus.background = getColor(from, to, root.context)
                arrowStatus.rotation = getArrowRotation(from, to)
            }

            date.text = dateFormatted

            body.setOnClickListener {
                onItemClickListener?.let { it(transactionUrl) }
            }

        }
    }

    private fun getColor(from: String, to: String, context: Context): ColorDrawable {
        var color: Int = context.getColor(R.color.white)
        if (from.equals(address.address, true))
            color = context.getColor(R.color.red)
        else if (to.equals(address.address, true))
            color = context.getColor(R.color.green)
        return ColorDrawable(color)
    }


    private fun getArrowRotation(from: String, to: String): Float {
        var rotation = 0f
        if (from.equals(address.address, true))
            rotation = 0f
        else if (to.equals(address.address, true))
            rotation = 180f
        return rotation
    }

    private var onItemClickListener: ((String) -> Unit)? = null

    fun setOnClickListener(listener: (url: String) -> Unit) {
        onItemClickListener = listener
    }


    override fun getItemCount(): Int =
//        differ.currentList.size
        if (differ.currentList.size > 100) 100 else differ.currentList.size

}