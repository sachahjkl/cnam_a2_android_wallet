package it.hjkl.wallet.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import it.hjkl.wallet.R
import it.hjkl.wallet.databinding.ListItemTokenBinding
import it.hjkl.wallet.model.Token
import it.hjkl.wallet.util.Utilities.roundToPrecision

class TokenListAdapter :
    RecyclerView.Adapter<TokenListAdapter.TokenViewHolder>() {

    inner class TokenViewHolder(val binding: ListItemTokenBinding) :
        RecyclerView.ViewHolder(binding.root)

    private val differCallback = object : DiffUtil.ItemCallback<Token>() {
        override fun areItemsTheSame(
            oldItem: Token,
            newItem: Token
        ): Boolean {
            return oldItem.tokenInfo.address == newItem.tokenInfo.address
        }

        override fun areContentsTheSame(
            oldItem: Token,
            newItem: Token
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TokenViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemTokenBinding.inflate(layoutInflater, parent, false)
        return TokenViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TokenViewHolder, position: Int) {
        val currentToken = differ.currentList[position]

        holder.binding.apply {
            label.text =
                if (currentToken.tokenInfo.name.isNullOrBlank())
                    holder.itemView.context.getString(R.string.no_name_token) else
                    currentToken.tokenInfo.fullName

            address.text = currentToken.tokenInfo.address

            val roundedValue = roundToPrecision(currentToken.value, 2)
            val roundedSolde = roundToPrecision(currentToken.balanceAdjusted, 2)

            valeur.text =
                holder.itemView.context.getString(R.string.balance, roundedValue.toString())

            solde.text = roundedSolde.toString()
            if (!currentToken.tokenInfo.image.isNullOrBlank()) {
                val imageUrl =
                    holder.binding.root.context.getString(R.string.base_url_ethplorer) + currentToken.tokenInfo.image
                Glide.with(root)
                    .load(imageUrl)
                    .into(logo)
            }

//            Attacher les event Listeners
            info.setOnClickListener {
                onDeleteClickListener?.let { it(currentToken) }
            }
            body.setOnClickListener {
                onItemClickListener?.let { it(currentToken) }
            }
            body.setOnLongClickListener {
                onItemLongClickListener?.let { it(currentToken) }
                false
            }
        }
    }

    private var onItemClickListener: ((Token) -> Unit)? = null

    fun setOnClickListener(listener: (Token) -> Unit) {
        onItemClickListener = listener
    }

    private var onDeleteClickListener: ((Token) -> Unit)? = null

    fun setOnDeleteListener(listener: (Token) -> Unit) {
        onDeleteClickListener = listener
    }

    private var onItemLongClickListener: ((Token) -> Unit)? = null

    fun setOnLongClickListener(listener: (Token) -> Unit) {
        onItemLongClickListener = listener
    }

    override fun getItemCount(): Int =
//        differ.currentList.size
        if (differ.currentList.size > 100) 100 else differ.currentList.size

}