package it.hjkl.wallet.adapter


import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.hjkl.wallet.databinding.ListItemLinkBinding

class LinksListAdapter :
    RecyclerView.Adapter<LinksListAdapter.LinkViewHolder>() {

    inner class LinkViewHolder(val binding: ListItemLinkBinding) :
        RecyclerView.ViewHolder(binding.root)

    data class Link(
        val url: String,
        val logo: Drawable?,
        val text: String
    )

    private val differCallback = object : DiffUtil.ItemCallback<Link>() {
        override fun areItemsTheSame(
            oldItem: Link,
            newItem: Link
        ): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(
            oldItem: Link,
            newItem: Link
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemLinkBinding.inflate(layoutInflater, parent, false)
        return LinkViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        val currentLink = differ.currentList[position]

        holder.binding.apply {
            body.setOnClickListener {
                onItemClickListener?.let { it(currentLink.url) }
            }
            logo.setImageDrawable(currentLink.logo)
            label.text = currentLink.text

        }
    }

    private var onItemClickListener: ((String) -> Unit)? = null

    fun setOnClickListener(listener: (String) -> Unit) {
        onItemClickListener = listener
    }


    override fun getItemCount(): Int = differ.currentList.size

}