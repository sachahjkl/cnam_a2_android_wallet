package it.hjkl.wallet.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.hjkl.wallet.databinding.ListItemAddressBinding
import it.hjkl.wallet.db.entitity.Address

class AddressListAdapter :
    RecyclerView.Adapter<AddressListAdapter.AddressViewHolder>() {


    inner class AddressViewHolder(val binding: ListItemAddressBinding) :
        RecyclerView.ViewHolder(binding.root)

    private val differCallback = object : DiffUtil.ItemCallback<Address>() {
        override fun areItemsTheSame(
            oldItem: Address,
            newItem: Address
        ): Boolean {
            return oldItem.address == newItem.address
        }

        override fun areContentsTheSame(
            oldItem: Address,
            newItem: Address
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemAddressBinding.inflate(layoutInflater, parent, false)
        return AddressViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        val address = differ.currentList[position]
        holder.apply {
            binding.label.text = address.label
            binding.address.text = address.address
            binding.supprimer.setOnClickListener {
                onDeleteClickListener?.let { it(address) }
            }
            binding.body.setOnClickListener {
                onItemClickListener?.let { it(address) }
            }
            binding.body.setOnLongClickListener {
                onItemLongClickListener?.let { it(address) }
                false
            }
        }
    }

    private var onItemClickListener: ((Address) -> Unit)? = null

    fun setOnClickListener(listener: (Address) -> Unit) {
        onItemClickListener = listener
    }

    private var onDeleteClickListener: ((Address) -> Unit)? = null

    fun setOnDeleteListener(listener: (Address) -> Unit) {
        onDeleteClickListener = listener
    }

    private var onItemLongClickListener: ((Address) -> Unit)? = null

    fun setOnLongClickListener(listener: (Address) -> Unit) {
        onItemLongClickListener = listener
    }

    override fun getItemCount(): Int = differ.currentList.size

}