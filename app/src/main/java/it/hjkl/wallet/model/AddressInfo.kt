package it.hjkl.wallet.model

import android.util.Log
import java.io.Serializable

data class AddressInfo(
    val ETH: ETH,
    val address: String,
    val countTxs: Int,
    val tokens: List<Token>?
) : Serializable {

    val balanceUSD: Double
        get() {
            var balanceUSDTokens = 0.0
            tokens?.let {
                for (token in it) {
                    balanceUSDTokens += token.value
                }

            }
            return balanceUSDTokens + ETH.value
        }
}