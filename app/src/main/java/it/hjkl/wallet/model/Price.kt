package it.hjkl.wallet.model

import java.io.Serializable

data class Price(
    val availableSupply: Double,
    val diff: Double,
    val diff30d: Double,
    val diff7d: Double,
    val marketCapUsd: Double,
    val rate: Double,
    val ts: Double,
    val volDiff1: Double,
    val volDiff30: Double,
    val volDiff7: Double,
    val volume24h: Double,
    val currency: String?,
) : Serializable