package it.hjkl.wallet.model

import java.io.Serializable

data class TokenInfo(
    val address: String?,
    val coingecko: String?,
    val decimals: Int?,
    val ethTransfersCount: Int?,
    val holdersCount: Int?,
    val image: String?,
    val issuancesCount: Int?,
    val facebook: String?,
    val lastUpdated: Int?,
    val name: String?,
    val owner: String?,
    val price: Any,
    val publicTags: List<String?>?,
    val reddit: String?,
    val symbol: String?,
    val telegram: String?,
    val totalSupply: String?,
    val twitter: String?,
    val txsCount: Int?,
    val website: String?,
    val transfersCount: Int?,
) : Serializable {

    val fullName: String
        get() {
            var localFullName = ""
            if (!name.isNullOrBlank())
                localFullName = name.toString()
            if (!symbol.isNullOrBlank()) {
                localFullName += " ($symbol)"
            }
            return localFullName.trim()
        }


}