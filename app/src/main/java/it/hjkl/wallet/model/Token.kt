package it.hjkl.wallet.model

import java.io.Serializable

data class Token(
    val balance: Double,
    val rawBalance: Double,
    val tokenInfo: TokenInfo,
    val totalIn: Int,
    val totalOut: Int
)  : Serializable {

    val value: Double
        get() {
            var value = 0.0
            when (tokenInfo.price) {
                !is Boolean -> {
                    val price = tokenInfo.price as Map<*, *>
                    val rate = price["rate"] as Double
                    value = balanceAdjusted.times(rate)
                }
            }
            return value
        }

    val balanceAdjusted: Double
        get() {
            var multiplier = 1.0
            tokenInfo.decimals?.let {
                repeat(it) {
                    multiplier *= 10
                }
            }
            return balance.div(multiplier)
        }
}
