package it.hjkl.wallet.model

import java.io.Serializable

data class Operation(
    val from: String, // 0x8e3bcc334657560253b83f08331d85267316e08a
    val timestamp: Long, // 1621335556
    val to: String, // 0xdfb091f812ea27ca58e8f556b252f245660cba87
    val tokenInfo: TokenInfo,
    val transactionHash: String,
    val type: String, // transfer
    val value: Double // 21785273000000000000000
) : Serializable{

    val valueAdjusted : Double get() {
        var multiplier = 1.0
        tokenInfo.decimals?.let {
            repeat(it) {
                multiplier *= 10
            }
        }
        return value.div(multiplier)
    }
}