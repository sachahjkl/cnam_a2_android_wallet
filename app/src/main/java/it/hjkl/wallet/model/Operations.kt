package it.hjkl.wallet.model

import java.io.Serializable

data class Operations(
    var operations: List<Operation>
)  : Serializable