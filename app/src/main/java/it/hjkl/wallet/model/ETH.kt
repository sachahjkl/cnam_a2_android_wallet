package it.hjkl.wallet.model

import java.io.Serializable

data class ETH(
    val balance: Double,
    val price: Price,
    val rawBalance: Double
)  : Serializable {
    val value get() = balance.times(price.rate)

}