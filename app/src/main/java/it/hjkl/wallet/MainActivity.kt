package it.hjkl.wallet

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import it.hjkl.wallet.adapter.AddressListAdapter
import it.hjkl.wallet.viewmodel.AddressListViewModel
import it.hjkl.wallet.databinding.ActivityMainBinding
import it.hjkl.wallet.databinding.DialogAddWalletBinding
import it.hjkl.wallet.db.entitity.Address
import kotlinx.coroutines.flow.collect
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMainBinding
    private val binding get() = _binding
    private val viewModel: AddressListViewModel by viewModels {
        AddressListViewModel.AddressListViewModelFactory((application as WalletApplication).repository)
    }
    private lateinit var addressListAdapter: AddressListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMainBinding.inflate(layoutInflater)
        addressListAdapter = AddressListAdapter()
        addressListAdapter.differ.submitList(emptyList())

        binding.rvAddresses.setHasFixedSize(true)
        binding.rvAddresses.adapter = addressListAdapter

        addressListAdapter.setOnDeleteListener { onDelete(it) }
        addressListAdapter.setOnClickListener { onItemClick(it) }
        addressListAdapter.setOnLongClickListener { share(it) }

        binding.fab.setOnClickListener {
            onFabClick()
        }

        lifecycleScope.launchWhenStarted {
            viewModel.addressesState.collect { state ->
                updateUI(state)
            }
        }

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
    }

    private fun share(address: Address) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, address.address)
        }
        val shareIntent = Intent.createChooser(sendIntent, address.label)
        startActivity(shareIntent)
    }

    private fun updateUI(state: AddressListViewModel.AddressListState) {
        when (state) {
            is AddressListViewModel.AddressListState.Success -> {
                setLoading(false)
                if (state.addresses.isEmpty()) {
                    binding.emptyText.isVisible = true
                    binding.emptyText.text = getString(R.string.pas_de_portefeuille)
                } else {
                    binding.emptyText.isVisible = false
                }
                addressListAdapter.differ.submitList(state.addresses)
            }
            is AddressListViewModel.AddressListState.Failure -> {
                setLoading(false)
                binding.emptyText.isVisible = true
                binding.emptyText.text = state.errorText
            }
            is AddressListViewModel.AddressListState.Loading -> {
                setLoading(true)
            }
            else -> Unit
        }
    }

    private fun onFabClick() {
        val builder = AlertDialog.Builder(this)
        val bindingDialog = DialogAddWalletBinding.inflate(layoutInflater)

        val pattern = Pattern.compile(getString(R.string.regex_eth_address))

        val dialog = builder.setView(bindingDialog.root)
            .setTitle(getString(R.string.ajout_portefeuille))
            .setPositiveButton(
                getString(R.string.ajouter), null
            )
            .setNegativeButton(getString(R.string.annuler), null).create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            with(bindingDialog) {
                val address = address.text.toString()
                val label = label.text.toString()
                if (address.isEmpty() || label.isEmpty()) {
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.empty_fields),
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (!pattern.matcher(bindingDialog.address.text.toString()).find()) {
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.invalid_address),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    viewModel.insertAddress(
                        Address(address, label)
                    )
                    dialog.dismiss()
                }
            }
        }
    }

    private fun onDelete(address: Address) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.suppr_wallet_message, address.label))
            .setPositiveButton(getString(R.string.supprimer)) { _, _ ->
                viewModel.deleteAddress(address)
            }.setNegativeButton(getString(R.string.annuler), null)
            .create().show()
    }

    private fun onItemClick(address: Address) {
        val intent = Intent(this, AddressInfoActivity::class.java)
        intent.putExtra("address", address)
        startActivity(intent)
    }

    private fun setLoading(loading: Boolean = true) {
        binding.apply {
            rvAddresses.isVisible = !loading
            progressBar.isVisible = loading
        }

    }

}

