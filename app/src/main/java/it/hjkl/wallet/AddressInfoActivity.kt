package it.hjkl.wallet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import it.hjkl.wallet.adapter.TokenListAdapter
import it.hjkl.wallet.viewmodel.AddressInfoViewModel
import it.hjkl.wallet.databinding.ActivityAddressInfoBinding
import it.hjkl.wallet.databinding.DialogChooseTokenDestinationBinding
import it.hjkl.wallet.db.entitity.Address
import it.hjkl.wallet.model.AddressInfo
import it.hjkl.wallet.model.Token
import it.hjkl.wallet.util.Utilities.roundToPrecision
import kotlinx.coroutines.flow.collect

class AddressInfoActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityAddressInfoBinding
    private val binding get() = _binding
    private val viewModel: AddressInfoViewModel by viewModels {
        AddressInfoViewModel.AddressInfoViewModelFactory((application as WalletApplication).repository)
    }
    private lateinit var adapter: TokenListAdapter
    private lateinit var address: Address

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityAddressInfoBinding.inflate(layoutInflater)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = TokenListAdapter()
        adapter.differ.submitList(emptyList())

        binding.rvTokens.adapter = adapter
        adapter.setOnClickListener { onItemClick(it) }

        setContentView(binding.root)

        address = intent.extras?.getSerializable("address") as Address

        binding.share.setOnClickListener {
            share(address)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.fetchAddressInfo(address.address).also {
                viewModel.addressInfoState.collect { state ->
                    updateUI(state)
                }
            }
        }

    }

    private fun share(address: Address) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, address.address)
        }
        val shareIntent = Intent.createChooser(sendIntent, address.label)
        startActivity(shareIntent)
    }

    private fun updateUI(state: AddressInfoViewModel.AddressInfoState) {
        when (state) {
            is AddressInfoViewModel.AddressInfoState.Loading -> {
                setLoading(true)
            }
            is AddressInfoViewModel.AddressInfoState.Success -> {
                setLoading(false)
                setUI(state.addressInfo)

            }
            is AddressInfoViewModel.AddressInfoState.Failure -> {
                setLoading(false)
                AlertDialog.Builder(this)
                    .setMessage(state.errorText)
                    .setNegativeButton("Retour", null)
                    .setOnDismissListener { finish() }
                    .create().show()
            }
            else -> Unit
        }

    }

    private fun setUI(addressInfo: AddressInfo) {
        with(binding) {
            val roundedBalanceUSD = roundToPrecision(addressInfo.balanceUSD, 2)
            val roundedValueETH = roundToPrecision(addressInfo.ETH.value, 2)
            label.text = address.label
            valeur.text =
                getString(R.string.balance, roundedBalanceUSD.toString())
            ether.text = addressInfo.ETH.balance.toString()
            etherValeur.text = getString(R.string.balance, roundedValueETH.toString())
            adapter.differ.submitList(addressInfo.tokens)
            setNoTokens(addressInfo.tokens.isNullOrEmpty())
        }

    }

    private fun setNoTokens(noTokens: Boolean = true) {
        binding.apply {
            emptyText.isVisible = noTokens
        }
    }

    private fun setLoading(loading: Boolean = true) {
        binding.apply {
            summary.isVisible = !loading
            rvTokens.isVisible = !loading
            progressBar1.isVisible = loading
            progressBar2.isVisible = loading
        }

    }

    private fun onItemClick(token: Token) {
        val binding = DialogChooseTokenDestinationBinding.inflate(layoutInflater)
        val dialog = AlertDialog.Builder(this)
            .setView(binding.root)
            .setPositiveButton(getString(R.string.fermer), null)
            .create()

        binding.plusInfos.setOnClickListener {
            goToMoreInfo(token)
            dialog.dismiss()
        }
        binding.operations.setOnClickListener {
            goToOperations(token)
            dialog.dismiss()
        }

        dialog.show()

    }

    private fun goToMoreInfo(token: Token) {
        val intent = Intent(this, TokenActivity::class.java)
        intent.putExtra("token", token)
        startActivity(intent)
    }

    private fun goToOperations(token: Token) {
//        Toast.makeText(
//            this,
//            getString(R.string.reste_boulot, token.tokenInfo.fullName),
//            Toast.LENGTH_SHORT
//        ).show()
        val intent = Intent(this, OperationsActivity::class.java)
        intent.putExtra("address", address)
        intent.putExtra("token", token)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}